import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from '../services/chat.service';
import { AlertController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  constructor(private chatService: ChatService, private router: Router,    private alertController: AlertController,) { }
  async signOut() {
    const toast = await this. alertController.create({
      message:'Logout successfully',
   
    
    });
 
    toast.present();
    this.chatService.signOut().then(() => {
      this.router.navigateByUrl('/login', { replaceUrl: true });
      
    });
    
  }
  
  ngOnInit() {
  }

}


