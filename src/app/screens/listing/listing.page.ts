import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { reduce } from 'rxjs/operators';
import { Category } from 'src/app/models/category.model';
import { Food } from 'src/app/models/food.model';
import { FoodService } from 'src/app/services/food.service';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.page.html',
  styleUrls: ['./listing.page.scss'],
})
export class ListingPage implements OnInit {
  categories: Category[] = [];
  foods: Food[] = [];

  constructor(private foodService: FoodService, private router: Router) {}

  ngOnInit() {
    this.getCategories();
    this.foods = this.foodService.getFoods();
  }

  getCategories() {
    this.categories = [
      {
        id: 1,
        label: 'Manicurist',
        image: 'assets/images/icons/manicurist.jpg',
        active: true,
      },
      {
        id: 2,
        label: 'Carpinter',
        image: 'assets/images/icons/carpinter.jpg',
        active: true,
      },
      {
        id: 3,
        label: 'Pest Control',
        image: 'assets/images/icons/pestcontrol.jpg',
        active: true,
      },
      {
        id: 4,
        label: 'View All',
        image: 'assets/images/icons/humburger.png',
        active: true, 
      },
    ];
  }

  goToDetailPage(id: number) {
    this.router.navigate(['detail', id]);
  }
}
